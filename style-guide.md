# Headings

```
H1
==

H2
--

H3
**

H4
!!
```


# Turtle code blocks
```
.. code-block:: turtle

 @prefix : <https://biomedit.ch/rdf/sphn-ontology/psss/> .

 <https://biomedit.ch/rdf/sphn-ontology/psss/>
        owl:versionIRI <https://biomedit.ch/rdf/ontology/psss/2021/3/> .

```


# Referencing other content of the readthedocs 

Rather than adding hard-coded links to other pages, such as,
```
text `CHOP <https://sphn-semantic-framework.readthedocs.io/en/latest/external_resources/chop.html>`_ text
```

it is recommended to use cross referencing (see the [official sphinx docs](https://docs.readthedocs.io/en/stable/guides/cross-referencing-with-sphinx.html), 
specifically, the [ref role section](https://docs.readthedocs.io/en/stable/guides/cross-referencing-with-sphinx.html#the-ref-role)).

> Cross referencing in Sphinx uses two components, references and targets.
>
>    * references are pointers in your documentation to other parts of your documentation.
>    * targets are where the references can point to.


[[source](https://docs.readthedocs.io/en/stable/guides/cross-referencing-with-sphinx.html)]


## Defining a target
To create a reference in the *introduction.rst* document to the *shacler.rst* 
document, we create a target label in *shacler.rst*. Referencing this label
will render the section below the label.

**shacler.rst**
```
.. _framework-shacler:

SHACLer: the SPHN SHACL Generator
=====================================

text text
```
## Defining a reference
**introduction.rst**
```
... text, for instance :ref:`framework-shacler`
```
-> will be rendered as 
```
... text, for instance SHACLer: the SPHN SHACL Generator
```

To render the same link with custom text:
**introduction.rst**
```
... text, for instance :ref:`custom text <framework-shacler>`
```
-> will be rendered as 
```
... text, for instance custom text
```
