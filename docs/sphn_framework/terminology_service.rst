.. _framework-terminology-service:

Terminology Service
===================

External terminologies
----------------------
If an ontology references existing external standard terminologies, such as 
disease or device classifications of interest in the context of SPHN, 
it needs to be represented in the RDF format. 
The DCC Terminology Service provides SPHN compatible, machine-readable versions 
of national (:ref:`external-terminologies-chop`, :ref:`external-terminologies-icd`) and international (:ref:`external-terminologies-snomed-ct`, :ref:`external-terminologies-loinc`, :ref:`external-terminologies-atc`, :ref:`external-terminologies-ucum`) 
terminologies and classifications in RDF formats (``.ttl`` or ``.owl``). 
This page describes the way DCC distributes the external terminologies. 
For further information on the external terminologies, see 
:ref:`here <external-terminologies>`.

The SPHN Terminology Service
----------------------------
The DCC developed the Terminology Service as a way to distribute external 
terminologies in RDF format without external dependencies and in compliance 
with the copyright statements of the single terminologies. 

The terminologies are available directly in the individual project spaces on 
the BioMedIT nodes. Furthermore, the DCC provides two modes of distribution, 
enabling projects and institutions to fetch the external terminologies in RDF 
format:

* via the Terminology Service at the `BioMedIT portal <https://portal.dcc.sib.swiss/>`_
* via a standalone `Terminology Server <https://terminology-server.dcc.sib.swiss/>`_ 
  that uses the `MinIO <https://min.io/>`_ object storage service.


.. list-table:: SPHN Terminology Service
   :header-rows: 1

   * -  
     - Terminology Service at the BioMedIT portal
     - Terminology Server
   * - URL
     - https://terminology.dcc.sib.swiss/
     - https://terminology-server.dcc.sib.swiss
   * - Access
     - SWITCH edu-ID
     - A dedicated account to be requested at dcc@sib.swiss
   * - Terminology format
     - Provides a single file per external terminology
     - Provides bundles of external terminologies
   * - Download modality
     - Web interface
     - Web interface or command-line interface 
   * - Intended users
     - Researchers
     - BioMedIT nodes, hospitals or other service providers   


The two modes of distribution are always synchronized and regularly provide 
updated versions of the external terminologies. 


Downloading terminology files
-----------------------------
Both modes of distribution allow for manually downloading the terminologies
via the web browser. An additional tool, the ``terminology-server-downloader``, 
enables automatically downloading terminologies from the Terminology Server 
via a command-line interface.
For instructions on downloading external terminology files from the Terminology 
Service, please see our user guide :ref:`user-guide-terminology-service`.

After the terminology files have been downloaded, they can be imported 
into RDF tools such as `Protégé <https://protege.stanford.edu/>`_ or 
`GraphDB <https://graphdb.ontotext.com/>`_.

Reference
---------
Krauss P, Touré V, Gnodtke K, Crameri K, Österle S. DCC Terminology Service—An 
Automated CI/CD Pipeline for Converting Clinical and Biomedical Terminologies in 
Graph Format for the Swiss Personalized Health Network. Applied Sciences. 
2021; 11(23):11311. 
`https://doi.org/10.3390/app112311311 <https://doi.org/10.3390/app112311311>`_
