.. _framework-sparqler:

SPARQLer
==========


SPARQL
------

SPARQL Protocol and RDF Query Language (SPARQL) is a query language 
defined by the W3C standard for querying RDF data. 
For further information on SPARQL, see :ref:`background-sparql`.


The ``SPARQLer`` tool
---------------------

The `SPHN SPARQL Queries Generator <https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology-documentation-visualization>`_ 
(``SPARQLer``) is a Python tool that accepts as input an SPHN RDF schema 
(Turtle format) and generates a series of SPARQL queries in a standard RDF/OWL 
format based on the concepts present in the schema. The tool is integrated into the SPHN Framework Schema Visualization Tool.

The automatically generated queries could be executed against a 
SPARQL endpoint by data managers and hospitals to retrieve the content 
of the RDF data in a tabular format.

.. note::

 - For more details about installation instruction and the usage of the tool,  
   check the `SPARQLer README.md <https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology-documentation-visualization>`_.
 - For more details about concepts flattening and the statistical queries, check the :ref:`userguide-sparqler`.


Available SPARQL Queries
---------------------

'Concept flattening' queries:

Composed of one query per concept/class. The output is a table that 'flattens' the data. Each query gives the list of resources defined for that concept together with the direct property values (see :ref:`userguide-sparqler`).

Statistical queries:

* Query per concept/class: Counting the instances per concept and predicate
* Query per concept/class: Minimum and maximum of predicates (dates or values)
* Query per concept/class: List and count of all used codes for hasCode
* Manually built queries, based on the hospIT statistics queries - simply adapted prefixes, class and property names to fit the SPHN RDF schema 2021-1


Availability and usage rights
------------------------------

© Copyright 2022, Personalized Health Informatics Group (PHI), SIB Swiss Institute of Bioinformatics.

The SPARQLer is available at (send request to DCC - dcc@sib.swiss):

* `Version 1.0 <https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-sparql-generator/-/tree/v1.0>`_ (supports SPHN 2021-1 and SPHN 2021-2).
* `Version 2.0 <https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-sparql-generator/-/tree/v2.0>`_ (supports SPHN 2021-1 and SPHN 2021-2).
* `Version 3.0 <https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology-documentation-visualization>`_ (supports SPHN 2021-1 to SPHN 2022-2)

The SPARQL queries for the SPHN ontology 2021-2 and 2022-2 are available at `sparql-queries <https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology-documentation-visualization>`_.

The SPARQLer is licensed under the `GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html>`_ 
(see `License <https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-rdf-quality-control/-/blob/master/LICENSE>`_).


For any question or comment, please contact the Data Coordination Center (DCC) at `dcc@sib.swiss <mailto:dcc@sib.swiss>`_.
