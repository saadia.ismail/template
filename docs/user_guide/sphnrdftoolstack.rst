SPHN RDF Toolstack
=========================

The SPHN RDF schema can be accessed through two different platforms: 

* The GitLab repository to store the ontology: https://git.dcc.sib.swiss/hospital-it/sphn-ontology,
* A GraphDB database local instance to visualize the ontology. 

Anyone with the GitLab link can view the repository. However, to get editing rights in the GitLab repository a request must be sent.  

✏ Please send an email to DCC at dcc@sib.swiss if you need access to these platforms or for any inquiry. 

Archiving in the GitLab repository 
-----------------------------------

The SPHN RDF schema is maintained in GitLab at https://git.dcc.sib.swiss/hospital-it/sphn-ontology. The GitLab repository mainly serve as a system for publication and archiving of the SPHN schema and its related documents (e.g., SHACL rules to enable the validation of data based on the schema). 

 

**Figure 1. Visualization of the SPHN ontology GitLab repository.**


The SPHN schema is accessible in GitLab through a Turtle file (extension: .ttl), which contains the description of the SPHN concepts and their relations defined in the RDF format. In addition, a README.md file describes the content of the GitLab repository.  

Each project is able to extend the SPHN schema to accommodate in-house terminologies that are not yet defined in the SPHN concepts. Therefore, the DCC GitLab (https://git.dcc.sib.swiss/) contains one repository per project extending the SPHN schema. These different repositories can be accessed upon request to the corresponding project responsible person. 


How to create a new repository?  
*******************************

The name of the project should follow the convention: ``<project name>-rdf-ontology``, for instance, 'frailty-rdf-ontology'. A new repository (or “Project” in GitLab) can be created under a Group. Projects extending the SPHN schema should create their schema under the hospital-it group (i.e., https://git.dcc.sib.swiss/hospital-it).  


How to give access to other contributors to the repository? 
***********************************************************

The following link guides on how to provide access to and manage contributors of a repository: https://docs.gitlab.com/ee/user/project/members/  

Visualizing and querying with GraphDB
---------------------------------------

The SPHN schema can also be visualized through other tools. We provide here a way to view the SPHN schema through an interface that also enable querying: GraphDB. GraphDB is a graph database that enables the visualization and querying of RDF files.  The free version is simple of installation and will create a local instance of GraphDB. A complete tutorial is available at: https://graphdb.ontotext.com/documentation/free/quick-start-guide.html.  

For a quick summary:  

1. Download GraphDB: https://www.ontotext.com/products/graphdb/graphdb-free/ 
2. Launch locally Graph DB. It should automatically open a web browser at http://localhost:7200.  
3. Create a new repository: Setup -> Repositories -> Click Create new repository. Fill in the fields.  
4. Import the SPHN schema in this newly created repository:  
  
  a. Import -> RDF -> Select your repository -> Click Upload RDF files
  b. Select the SPHN .ttl file -> Click Import 
  c. Set the Base IRI to the prefix defined in the SPHN schema (e.g., http://sphn.ch/rdf/ontology/2021.1) 

It is now possible to explore the schema under ``Explore`` or through the building of SPARQL queries under ``SPARQL``. 
