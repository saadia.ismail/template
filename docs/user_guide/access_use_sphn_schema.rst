.. _user-guide-access-use:

Access, read and use of SPHN content
=======================================

The following is a general guide describing how to access, read and use the SPHN Dataset and the SPHN RDF schema.

Access the SPHN Dataset and the RDF schema
-------------------------------------------

The latest released version of the SPHN Dataset is available as an Excel file at: https://sphn.ch/document/sphn-dataset/.
Previous versions of the SPHN Dataset are archived at: https://sphn.ch/services/documents/archive/.

The latest version of the SPHN RDF schema is available at: https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/. 
This RDF schema does include links to other external ontologies such as SNOMED CT. These external terminologies in RDF are in the `BioMedIT Portal <https://portal.dcc.sib.swiss/>`_ or can be fetched through the `minIO service <https://terminology-server.dcc.sib.swiss/minio>`_ (read more :ref:`here <framework-terminology-service>`). 
The SPHN RDF schema is browsable and visualizable at: `https://biomedit.ch/rdf/sphn-ontology/sphn <https://biomedit.ch/rdf/sphn-ontology/sphn>`_.


Read and use of the SPHN Dataset
---------------------------------
The SPHN Dataset, provided as an Excel file contains information about concepts. For detailed information on single pieces of information for each concept please refer to the section :ref:`dataset-specification`.
One of the guiding principles of the SPHN Dataset is the reuse of concepts. When reading the SPHN Dataset please keep in mind that a type written in Title case means that composedOfs of this concept (stated as type) are part of the information you can share with this concept.

As an example consider a project interested in the medication Novalgin® and wanting to know what information about Novalgin® it can expect to be represented in the SPHN Dataset.

The SPHN Dataset contains the concept Drug.

.. list-table:: Table 1. Concept Drug in the SPHN Dataset.
   :widths: 20 20 40 20 
   :header-rows: 1

   * - 
     - 
     - description
     - type
   * - concept
     - Drug
     - medication that can be given to the patient
     - 
   * - composedOf
     - product code
     - code, name, coding system and version representing the drug, e.g. GTIN
     - Code
   * - composedOf
     - active ingredient
     - pharmaceutically active component of a drug
     - Substance
   * - composedOf
     - inactive ingredient
     - inert ingredients or excipients, and generally have no pharmacological effect
     - Substance 

Reading this concept, the type written in Title case is found three times:

* Code, for the product code
* Substance, for the active ingredient
* Substance, again for the inactive ingredient(s)

The information captured in those types can be found in the concepts Code and Substance. As an illustration, the following picture shows the composedOfs of these concepts integrated into a single view.

.. image:: ../images/Concept.svg
   :width: 600px
   :align: center
   :alt: Concept view combined

**Figure 1. Concept Drug with all elements of information integrated into one view.**

For Novalgin®, the figure above shows that the following information can be expressed:

- **product code**:

  * **identifier**: 7680169520286
  * **name**: Novalgin®
  * **coding system and version**: GTIN

- **active ingredient**:

  * **identifier**: N02BB02
  * **name**: metamizole sodium
  * **coding system and version**: ATC-2020

For Novalgin®, the generic name doesn't have to be taken into account as a separate composedOf, as the ATC code is known for this drug (and therefore the generic name comes with the composedOf name which belongs to the concept Code). However, you may have the situation that you need to express an investigational medicinal product (IMP, https://www.ema.europa.eu/en/glossary/investigational-medicinal-product), that does not have any GTIN code, and any ATC code yet. In such case, the generic name can be provided in free text (string), and the composedOfs related to Code are not applicable. Same for the inactive ingredients, if not specially needed by the project, this information may not be instantiated.

Read and use of the SPHN RDF schema
------------------------------------
The SPHN RDF schema is provided in the textual syntax format known as Turtle and can be loaded into any ontology editor which supports the import of such files. 

One option is to load the schema into `Protégé <https://protege.stanford.edu/>`_. Protégé is a desktop-based, free and open-source ontology editor that contains most of the features needed to explore the SPHN RDF schema and related external resources used.

Loading the SPHN RDF schema in Protégé
****************************************

1. Install Protégé on your computer  (https://protege.stanford.edu/products.php#desktop-protege)
2. Increase the heap size (at least 10000M, more can be provided if the ontology fails to load) by following instructions here: https://protegewiki.stanford.edu/wiki/Setting_Heap_Size
3. Start Protégé
4. ``File`` --> ``Open`` --> select the SPHN RDF schema to load
5. While loading the SPHN RDF schema, the tool will ask you to import missing external ontologies (one by one): Click ‘Yes’ and select the appropriate terminology file (in the example shown below ``snomedct-2021-03-04.owl``).


.. image:: ../images/protege_imports.*
   :height: 300
   :align: center
   :alt: Protege imports

6. Repeat Step 5 for all missing terminologies. Once the terminologies are loaded, the SPHN RDF schema is ready to be reviewed. 


The SPHN RDF schema, together with the imported external terminologies can now be explored. 
For extending or modifying the SPHN RDF schema to fit the needs of a specific SPHN project, follow instructions at: :ref:`userguide-project-ontology`.

Alternatively, a .zip file is provided (access to this bundle must be requested to the DCC) which contains the SPHN RDF schema, the external terminologies and a catalog.xml file that is read by Protégé to import the necessary terminologies in the interface directly.