.. _user-guide-sparql-query:

Query data with SPARQL
=======================

.. _querying data with SPARQL:

.. note:: 
 To find out more watch the `Querying Data with SPARQL Training <https://sphn.ch/training/querying-data-with-sparql/>`_.
 For an introduction to SPARQL, visit the :ref:`SPARQL Background <background-sparql>` section.
 
Target Audience
----------------

This document is mainly intended for researchers who are interested 
in querying their data for data profiling or exploration purposes.
The document provides the following information:

- how to setup the inference in GraphDB before running any query
- how to build queries for getting statistical knowledge about data 
- some examples of queries for exploring the content of a particular data.


Data profiling querying - Summary statistics
---------------------------------------------

A set of statistical queries is provided for making an initial evaluation 
of the data content and quality: 
https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/tree/master/quality_assurance/. 
These statistical queries can be run in any triplestore that enables the querying of RDF data 
(e.g. GraphDB, Jena) by simply copy-pasting the content of the queries into the querying field. 
They are integrated in the :ref:`Quality Check tool <framework-quality-assurance>`.

More information on how to run SPARQL can be found in 
`Training Video <https://sphn.ch/training/querying-data-with-sparql/>`_ 
and in the :ref:`user guide <querying data with SPARQL>`.

The queries are mostly built in the following manner:

First, prefixes used in the queries need to be defined to 
facilitate the (human) reading and writing of the query:

.. code-block:: sparql

 PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
 PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
 PREFIX sphn:<https://biomedit.ch/rdf/sphn-ontology/sphn#>
 PREFIX resource:<https://biomedit.ch/rdf/sphn-resource/>
 PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
 PREFIX psss:<https://biomedit.ch/rdf/sphn-ontology/psss#>
 PREFIX spo:<https://biomedit.ch/rdf/sphn-ontology/spo#>

Then the type of query should be specified. 
Here, the query form ``SELECT`` is used for specifying variables 
expected to be provided in the results

.. note::
 Variables start with a question mark ``?`` in SPARQL.

.. code-block:: sparql

 SELECT ?concept (COUNT(?resource) AS ?sphn_concepts_resources)
  (COUNT(distinct ?subject) as ?subject_cnt)
  (COUNT(distinct ?case) as ?case_cnt)
  (COUNT(distinct ?provider) as ?provider_cnt)

In this example, five variables will be given as result: 

* a concept, 
* the count of resource for that concept as well as 
* the count of subjects, 
* cases and 
* providers for that concept.

Following the definition of variables, 
the graph pattern of interest must then be specified with the clause ``WHERE``:

.. code-block:: sparql

 WHERE { 
  { ?concept rdfs:subClassOf+ sphn:SPHNConcept } UNION { ?concept rdfs:subClassOf+ psss:PSSSConcept } UNION { ?concept rdfs:subClassOf+ spo:SPOConcept } .
  ?resource a ?concept .
  optional {?resource sphn:hasDataProviderInstitute ?provider}
  optional {?resource sphn:hasSubjectPseudoIdentifier ?subject}
  optional {?resource sphn:hasAdministrativeCase ?case}

Here, the query searches for patterns where a 
``resource`` is defined with the RDF class type ``concept``. 
This ``concept`` class must be at least either a subClass of SPHN, PSSS or SPO. 
And finally, the resource can optionally be connected to:
* a ``data provider institute`` or
* a ``subject pseudo identifier`` or
* a ``administrative case``.

Next it is possible to filter out for graph patterns that are not of 
interest and that should not be returned in the results, 
with the clause ``FILTER NOT EXISTS``: 

.. code-block:: sparql

  FILTER NOT EXISTS {?concept rdfs:subClassOf sphn:ValueSet}


Here, the query filters out classes that are subClasses of the SPHN class ``ValueSet``.

Finally, it is possible to end with some ``query modifiers``, 
here the query ends by grouping results for a given variable:

.. code-block:: sparql

 } group by ?concept order by desc(?sphn_concepts_resources)

In this example, the results are grouped by concepts retrieved.


For data following project-specific ontologies, 
it may be necessary to adjust the queries to fit the search for certain elements. 
For any help, please contact the DCC at dcc@sib.swiss.
       

.. _query examples:

Data exploration querying
-------------------------

The following examples are meant to showcase ways to write queries
for exploring the content of data in RDF following the SPHN RDF schema.
They are based on the mock-data introduced in previous sections 
(see :ref:`mock-data description <mock-data description>` 
and :ref:`loading instructions <loading instructions>`) 
and comply with the 
`SPHN RDF schema version 2021.2 <https://git.dcc.sib.swiss/sphn-semantic-framework/sphn-ontology/-/tags/2021-2>`_.


1. Patients allergic to Peanuts
*******************************

Here, the question is which patients are allergic to the peanuts. 
To address this question, the graph pattern to be matched by the query 
is shown in `Figure 1`_. In this graph, information about the 
patient is denoted by an instance of a :code:`sphn:SubjectPseudoIdentifier` class. 
It is possible to find out which allergy episode links to this patient by matching 
an instance of an :code:`sphn:AllergyEpisode` class linked by the 
:code:`sphn:hasSubjectPseudoIdentifier` property. 
Similarly, the substance causing the allergy episode can be found by 
matching an instance of a :code:`sphn:Substance` class linked by a 
:code:`sphn:hasSubstance` property. 

In order to search for patients allergic to Peanuts, 
the substance can be fixed to `Peanut <http://snomed.info/id/762952008>`_ 
by matching instances of :code:`sphn:Substance` class linked to 
:code:`snomed:762952008` by the :code:`sphn:hasSubstanceCode` property. 

.. _Figure 1:
.. image:: ../images/sparql/Figure_graph_Patients_allergic_to_Peanuts.png
   :align: center
   :alt: Graph for Patients allergic to Peanuts.
   
**Figure 1:  Graph for Patients allergic to Peanuts.**


`Figure 2`_ visualizes the graph pattern used for matching Patients allergic to Peanuts, 
and is followed by the corresponding SPARQL query implementation. 

The SPARQL query, after defining the prefixes, retrieves distinct patients 
(:code:`?patient` variable of interest). 
The graph pattern starts by stating that the patients must be of type 
:code:`sphn:SubjectPseudoIdentifier`. 
Next, the :code:`?allergy_episode` variable representing the 
:code:`sphn:AllergyEpisode`, a :code:`?substance` variable representing 
the :code:`sphn:Substance` class, and a :code:`?substance_code` 
variable of type :code:`snomed:762952008` are defined. 

In order to get the data, one still needs to link all of these variables together. 
To that end, :code:`?allergy_episode` is linked to :code:`?patient` 
through the :code:`sphn:hasSubjectPseudoIdentifier` property, 
and to :code:`?substance` through the :code:`sphn:hasSubstance` property. 
Finally, :code:`?substance` is linked to :code:`?substance_code` through 
the :code:`sphn:hasSubstanceCode` property. Running this query in a given 
triplestore will retrieve any data (i.e., list of patients) matching this 
graph pattern (see `Figure 3`_ for an excerpt of the results).

.. _Figure 2:
.. image:: ../images/sparql/Figure_diagram_Patients_allergic_to_Peanuts.png
   :align: center
   :alt: Diagram complementing the SPARQL query for Patients allergic to Peanuts.
   
**Figure 2: Diagram complementing the SPARQL query for Patients allergic to Peanuts.**



.. code-block:: sparql

 PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
 PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
 PREFIX sphn:<https://biomedit.ch/rdf/sphn-ontology/sphn#>
 PREFIX resource:<https://biomedit.ch/rdf/sphn-resource/>
 PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
 PREFIX snomed: <http://snomed.info/id/> 
 
 SELECT distinct ?patient
 WHERE {
     ?patient a sphn:SubjectPseudoIdentifier .
     ?allergy_episode a sphn:AllergyEpisode .
     ?substance a sphn:Substance .
     
     ?allergy_episode sphn:hasSubjectPseudoIdentifier ?patient .
     ?allergy_episode sphn:hasSubstance ?substance .
     ?substance sphn:hasSubstanceCode ?substance_code .
 
     ?substance_code a snomed:762952008 .
 }         

.. _Figure 3:
.. image:: ../images/sparql/Figure_results_Patients_allergic_to_Peanuts.png
   :align: center
   :alt: Results of running the SPARQL query for Patients allergic to Peanuts in GraphDB on the mock-data.
   
**Figure 3: Excerpt of results of running the SPARQL query for Patients allergic to Peanuts in GraphDB on the mock-data.**

By modifying the above SPARQL query with the 
:code:`COUNT(distinct ...)` statement (see the following code block), 
it is possible to determine the exact count of the matched patients. 
For example, evaluating this query in GraphDB with the mock-data used throughout 
this guide produces the result shown in `Figure 4`_. 
Note the use of the :code:`FILTER(...)` statement to retrieve only labels from SNOMED CT.

.. code-block:: sparql

  SELECT (COUNT(distinct ?patient) AS ?patients) ?snomed_code ?label
  WHERE {
      ?patient a sphn:SubjectPseudoIdentifier .
      ?allergy_episode a sphn:AllergyEpisode .
      ?substance a sphn:Substance .
      
      ?allergy_episode sphn:hasSubjectPseudoIdentifier ?patient .
      ?allergy_episode sphn:hasSubstance ?substance .
      ?substance sphn:hasSubstanceCode ?substance_code .
      
      ?substance_code a snomed:762952008 .
      ?substance_code rdf:type ?snomed_code .
      ?snomed_code rdfs:label ?label .
      
      FILTER(strStarts(str(?snomed_code), "http://snomed.info/id/"))
  } GROUP BY ?snomed_code ?label 

.. _Figure 4:
.. image:: ../images/sparql/Figure_count_results_Patients_allergic_to_Peanuts.png
   :align: center
   :alt: Results of running the SPARQL query for Patients allergic to Peanuts in GraphDB on the mock-data.
   
**Figure 4: Results of running the modified SPARQL query for Patients allergic to Peanuts in GraphDB on the mock-data.**

2. Patients allergic to Pulse Vegetable
****************************************

Here, the question is which patients are allergic to pulse vegetable. 
We do not find this information directly in the data, 
as the data is often collected at a more granular level e.g. allergy to lentils or beans or beansprouts. 
Therefore, the hierarchical structure of Pulse Vegetable from SNOMED CT needs to be considered, 
as shown in `Figure 5`_.  
However, it is not needed to query individually for all levels 
in order to get all patients that are allergic to Pulse Vegetable. 
Thanks to the RDF graph structure and the hierarchy of SNOMED CT, 
the query can be done in a more straightforward way.

.. _Figure 5:
.. image:: ../images/sparql/Figure_hierarchy_pulse_vegetable.png
   :align: center
   :alt: Hierarchical structure of Pulse Vegetable from SNOMED CT.
   
**Figure 5: Hierarchical structure of Pulse Vegetable from SNOMED CT.**

Query option 1: reasoning without RDF inference
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

**List of patients**

`Figure 6`_ visualizes the graph pattern used for matching Patients allergic to Pulse vegetable, and is followed by an implementation enabling reasoning with SPARQL query (Note: without inference turned on).

.. _Figure 6:
.. image:: ../images/sparql/Figure_diagram_Patients_allergic_to_Pulse_Vegetable.png
   :align: center
   :alt: Diagram complementing the SPARQL query for Patients allergic to Pulse Vegetable.
   
**Figure 6: Diagram complementing the SPARQL query for Patients allergic to Pulse Vegetable.**

The query, shown in `Figure 7`_ , is in large part, 
up to and including the row with the :code:`?substance sphn:hasSubstanceCode ?substance_code` 
triple, same as in the previous example in `Figure 4`_. 
The difference occurs in the statements right after: 

* the variable :code:`?pulse_veg_and_descendants` is introduced, 
  representing the Pulse Vegetable causing the allergy
* the :code:`?pulse_veg_and_descendants` is defined as being 
  a subclass of the :code:`snomed:227313005` code using the :code:`rdfs:subClassOf*` 
  property. Note that the :code:`*` following the :code:`rdfs:subClassOf` 
  will look into all the nested levels from Pulse Vegetable 
  (i.e., not only Beansprouts, Peanut, and other concepts that are immediate subclasses 
  of :code:`snomed:227313005`, but also Peanut Butter, Salted Peanut, etc.).

Running this query in a given triplestore will retrieve any data 
(i.e., list of patients) matching this graph pattern 
(see `Figure 7`_ for an excerpt of the results).

.. code-block:: sparql
   
  PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
  PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
  PREFIX sphn:<https://biomedit.ch/rdf/sphn-ontology/sphn#>
  PREFIX resource:<https://biomedit.ch/rdf/sphn-resource/>
  PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
  PREFIX snomed: <http://snomed.info/id/>
  
  SELECT DISTINCT ?patient
  WHERE {
      ?patient a sphn:SubjectPseudoIdentifier .
      ?allergy_episode a sphn:AllergyEpisode .
      ?substance a sphn:Substance .
  
      ?allergy_episode sphn:hasSubjectPseudoIdentifier ?patient .
      ?allergy_episode sphn:hasSubstance ?substance .
      ?substance sphn:hasSubstanceCode ?substance_code .
      ?substance_code a ?pulse_veg_and_descendants .
      ?pulse_veg_and_descendants rdfs:subClassOf* snomed:227313005 .
  }

.. _Figure 7:
.. image:: ../images/sparql/Figure_results_Patients_allergic_to_Pulse_Vegetable.png
   :align: center
   :alt: Excerpt of results of running the SPARQL query for Patients allergic to Pulse Vegetable in GraphDB on the mock-data.
   
**Figure 7: Excerpt of results of running the SPARQL query for Patients allergic to Pulse Vegetable in GraphDB on the mock-data.**

**Count of patients**

By modifying the above SPARQL query with the :code:`COUNT(distinct ...)` 
statement (see the following code block), it is possible to determine 
the exact count of patients allergic to Pulse Vegetable grouped by labels 
of annotated substance they are allergic to. 
For example, evaluating this query in GraphDB with the mock-data used throughout 
this guide produces the result shown in `Figure 8`_. 
Note the use of the :code:`FILTER(...)` statement to retrieve only labels from SNOMED CT.

.. code-block:: sparql
   
  PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
  PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
  PREFIX sphn:<https://biomedit.ch/rdf/sphn-ontology/sphn#>
  PREFIX resource:<https://biomedit.ch/rdf/sphn-resource/>
  PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
  PREFIX snomed: <http://snomed.info/id/>
  
  SELECT (COUNT (DISTINCT ?patient) AS ?patients) ?snomed_code ?label
  WHERE {
      ?patient a sphn:SubjectPseudoIdentifier .
      ?allergy_episode a sphn:AllergyEpisode .
      ?substance a sphn:Substance .
  
      ?allergy_episode sphn:hasSubjectPseudoIdentifier ?patient .
      ?allergy_episode sphn:hasSubstance ?substance .
      ?substance sphn:hasSubstanceCode ?substance_code .
  
      ?substance_code a ?pulse_veg_and_descendants .
      ?pulse_veg_and_descendants rdfs:subClassOf* snomed:227313005 .
  
      ?substance_code rdf:type ?snomed_code .
      ?snomed_code rdfs:label ?label .
      FILTER(strStarts(str(?snomed_code), "http://snomed.info/id/"))
  
  } GROUP BY ?snomed_code ?label


.. _Figure 8:
.. image:: ../images/sparql/Figure_count_results_Patients_allergic_to_Pulse_Vegetable.png
   :align: center
   :alt: Results of running the modified SPARQL query for Patients allergic to Pulse Vegetable in GraphDB on the mock-data.
   
**Figure 8: Results of running the modified SPARQL query for Patients allergic to Pulse Vegetable in GraphDB on the mock-data.**  

.. _rdf-reasoning:

3. RDF reasoning
*****************

RDF reasoning enables the computer to deduce knowledge based on 
provided information and using some logical statements. 
For example, given the information in an ontology on hierarchies 
(Class vs. Subclass; Property vs Subproperty) and in the provided data, 
the computer is able to do some reasoning with respect to that hierarchy. 
Two such examples are shown in `Figure 9`_:

* based on information coming from the ontology that HeartRate 
  is a Subclass of Measurement, and that data X is a HeartRate, 
  the computer is able to infer that X is a Measurement.
* based on information coming from the ontology that hasInsertionSite 
  is a Subproperty of hasBodySite, and that data X hasInsertionSite Arm, 
  the computer is able to infer that X hasBodySite Arm.

In general, patients can have information annotated at different levels of granularity. 
As mentioned previously, it is not necessary to query individually for all levels of 
information to get patients that match certain criteria thanks to the RDF graph 
structure and the hierarchical knowledge provided by SNOMED CT.

.. _Figure 9:
.. image:: ../images/sparql/Figure_RDF_reasoning.png
   :align: center
   :alt: Example of RDF reasoning.
   
**Figure 9: Example of RDF reasoning.**

.. _query LOINC WBC:


4. Patients allergic to Pulse Vegetable (inference turned on)
*************************************************************

Setup inference in GraphDB
---------------------------

Inference enables to deduce new knowledge from existing information, which 
constitutes one of the main strength of the RDF data representation 
(see section about :ref:`rdf-reasoning`).
Before writing queries in GraphDB, it is important to setup the tool properly 
for enabling such inference to be done, when needed. 
Here, we demonstrate how to setup inference in GraphDB. 

For an overview of the options to load RDF data into GraphDB, 
please refer to :ref:`loading instructions <loading instructions>` or GraphDB’s 
`documentation <https://graphdb.ontotext.com/documentation/standard/loading-data.html>`_.

Creating a new repository
*************************

In order to be able to use inference capabilities of GraphDB, 
inference needs to be enabled when creating a new repository, 
as this currently can not be done afterwards. 
Note that a default inference is already enabled, as shown in `Figure 10`_.

.. _Figure 10:

.. image:: ../images/sparql/Figure_GraphDB_inference_create_repository.png
   :align: center
   :alt: Creating a new repository with default inference enabled.
   
**Figure 10: Creating a new repository with default inference enabled.**

User settings
*************

Following the creation of the repository with inference enabled, 
one can choose to exclude/include inferred data in results by selecting 
the corresponding option in the SPARQL editor settings (see `Figure 11`_ 
and GraphDB `documentation <https://graphdb.ontotext.com/documentation/standard/application-settings.html>`_ 
for more information).

.. _Figure 11:

.. image:: ../images/sparql/Figure_GraphDB_inference_user_settings.png
   :align: center
   :alt: One can choose to exclude/include inferred data in results by selecting the corresponding option in the SPARQL editor settings.

**Figure 11: One can choose to exclude/include inferred data in results by selecting the corresponding option in the SPARQL editor settings.**

SPARQL Editor
*************

The GraphDB SPARQL editor allows to include or exclude inferred statements 
in the results by clicking the :code:`>>` icon, as shown in `Figure 12`_ 
(see GraphDB `documentation <https://graphdb.ontotext.com/documentation/standard/querying-data.html>`_ 
for more information).

.. _Figure 12:

.. image:: ../images/sparql/Figure_GraphDB_inference_arrows.png
   :align: center
   :alt: Enable inclusion

**Figure 12: Enabling inclusion (both elements of the arrow icon are a solid line) of inferred statements in the results in GraphDB built in SPARQL editor.**

.. note:: 
 GraphDB `System Statements <https://graphdb.ontotext.com/documentation/standard/devhub/system-statements.html>`_ 
 can be used to disable inference from within a SPARQL query, and also to enable it again 
 (assuming the inference was enabled during the repository creation).


.. warning:: 
 During offline bulk loading of data, no inference is enabled and GraphDB inference 
 settings are ignored (see GraphDB  `documentation <https://graphdb.ontotext.com/documentation/free/loading-data-using-the-loadrdf-tool.html>`_  
 for further information).



**List of patients**

Using the reasoning possibilities, the query to retrieve patients 
allergic to Pulse Vegetable can be simplified. 
In comparison to when the inference is off, 
when inference is turned on the main difference occuring in the query 
statement regarding the pulse vegetables is: 

* the :code:`?substance_code a snomed:227313005 .` 
  line results again in retrieving all the nested levels from Pulse Vegetable,  
  this time, however, without the need of using :code:`rdfs:subClassOf`.

Running this query in a given triplestore with inference turned on will 
retrieve the desired list of patients, as exemplified in  `Figure 13`_ 
(Note the arrows on the right side of the editor that can be used to turn inference ON/OFF).

.. code-block:: sparql

    PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
    PREFIX sphn:<https://biomedit.ch/rdf/sphn-ontology/sphn#>
    PREFIX resource:<https://biomedit.ch/rdf/sphn-resource/>
    PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
    PREFIX snomed: <http://snomed.info/id/>
    
    SELECT DISTINCT ?patient
    WHERE {
        ?patient a sphn:SubjectPseudoIdentifier .
        ?allergy_episode a sphn:AllergyEpisode .
        ?substance a sphn:Substance .
        
        ?allergy_episode sphn:hasSubjectPseudoIdentifier ?patient .
        ?allergy_episode sphn:hasSubstance ?substance .
        ?substance sphn:hasSubstanceCode ?substance_code .
        ?substance_code a snomed:227313005 .
    }

.. _Figure 13:
.. image:: ../images/sparql/Figure_results_Patients_allergic_to_Pulse_Vegetable_with_inference.png
   :align: center
   :alt: Excerpt of results of running the SPARQL query for Patients allergic to Pulse Vegetable with inference turned on in GraphDB on the mock-data.
   
**Figure 13: Excerpt of results of running the SPARQL query for Patients allergic to Pulse Vegetable with inference turned on in GraphDB on the mock-data.**    

**Count of patients**

By modifying the above SPARQL query with the :code:`COUNT(distinct ...)` 
statement (see the following code block) it is possible to determine the exact 
count of patients allergic to Pulse Vegetable grouped by labels of annotated 
substance they are allergic to, and with additional inference. 
For example, evaluating this query in GraphDB with the mock-data used throughout 
this guide and inference turned on produces the result shown in `Figure 14`_. 
Note that, with inference turned on, the count of patients allergic to Pulse Vegetable 
has increased to 380, which is a more accurate statement regarding allergy to any type of Pulse Vegetable.

.. code-block:: sparql

    PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
    PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
    PREFIX sphn:<https://biomedit.ch/rdf/sphn-ontology/sphn#>
    PREFIX resource:<https://biomedit.ch/rdf/sphn-resource/>
    PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
    PREFIX snomed: <http://snomed.info/id/>
    
    SELECT (COUNT (DISTINCT ?patient) as ?patients) ?snomed_code ?label
    WHERE {
        ?patient a sphn:SubjectPseudoIdentifier .
        ?allergy_episode a sphn:AllergyEpisode .
        ?substance a sphn:Substance .
    
        ?allergy_episode sphn:hasSubjectPseudoIdentifier ?patient .
        ?allergy_episode sphn:hasSubstance ?substance .
        ?substance sphn:hasSubstanceCode ?substance_code .
        ?substance_code a snomed:227313005 .
        ?substance_code rdf:type ?snomed_code .
        ?snomed_code rdfs:label ?label .
        FILTER(strStarts(str(?snomed_code), "http://snomed.info/id/"))
    } GROUP BY ?snomed_code ?label

.. _Figure 14:
.. image:: ../images/sparql/Figure_count_results_Patients_allergic_to_Pulse_Vegetable_with_inference.png
   :align: center
   :alt: Results of running the modified SPARQL query for Patients allergic to Pulse Vegetable with inference turned on in GraphDB on the mock-data.
   
**Figure 14: Results of running the modified SPARQL query for Patients allergic to Pulse Vegetable with inference turned on in GraphDB on the mock-data.**  


5. Patient with measurements of Leukocytes in Blood by Automated count (LOINC 6690-2)
*************************************************************************************

Here, the question is which patients have had a lab test done identified by a specific LOINC code. 
To address this question, the graph pattern to be matched by the query is shown in `Figure 15`_. 
In this graph pattern, instances of a :code:`sphn:LabResult` class are linked by the 
:code:`sphn:hasSubjectPseudoIdentifier` property to instances of a :code:`sphn:SubjectPseudoIdentifier` class. 
Patients that had `measurements of Leukocytes in Blood by Automated count <https://loinc.org/rdf/6690-2>`_ 
are queried by matching instances of :code:`sphn:LabResult` class linked by the 
:code:`sphn:hasLabResultLabTestCode` property to :code:`loinc:6690-2`.


.. _Figure 15:
.. image:: ../images/sparql/Figure_Patient_with_measurements_of_Leukocytes_in_Blood_by_Automated_count.png
   :align: center
   :alt: : Graph for Patient with measurements of Leukocytes in Blood by Automated count.
   
**Figure 15: Graph for Patient with measurements of Leukocytes in Blood by Automated count (LOINC 6690-2).**

`Figure 16`_ visualizes the graph pattern used for matching Patients with measurements of Leukocytes in Blood by Automated count, and is followed by the corresponding SPARQL query implementation. 

Similar as in the previous example, the SPARQL query retrieves distinct patients, 
and the graph pattern starts by stating that the patients must be of type 
:code:`sphn:SubjectPseudoIdentifier`. 
Next, the :code:`?lab_res` variable representing the :code:`sphn:LabResult`, 
and a :code:`?test_code` variable of type :code:`loinc:6690-2` are defined. 

In order to get the data, the :code:`?lab_res` is linked to :code:`?patient` 
through the :code:`sphn:hasSubjectPseudoIdentifier` property, and to 
:code:`?test_code` through the :code:`sphn:hasLabResultLabTestCode` property. 
Running this query in a given triplestore will retrieve any data 
(i.e., list of patients) matching this graph pattern. 
For example, evaluating this query in GraphDB with the mock-data used throughout 
this guide will retrieve 253 patients annotated with having a lab test code 
`measurements of Leukocytes in Blood by Automated count <https://loinc.org/rdf/6690-2>`_ 
(see `Figure 16`_ for an excerpt of the results).

.. _Figure 16:
.. image:: ../images/sparql/Diagram_Patient_with_measurements_of_Leukocytes_in_Blood_by_Automated_count.png
   :align: center
   :alt: : Diagram complementing the SPARQL query for Patient with measurements of Leukocytes in Blood by Automated count.
   
**Figure 16: Diagram complementing the SPARQL query for Patient with measurements of Leukocytes in Blood by Automated count.**

.. code-block:: sparql

  PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#>
  PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
  PREFIX sphn:<https://biomedit.ch/rdf/sphn-ontology/sphn#>
  PREFIX resource:<https://biomedit.ch/rdf/sphn-resource/>
  PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
  PREFIX loinc: <https://loinc.org/rdf/>
  
  SELECT distinct ?patient
  WHERE {
      ?patient a sphn:SubjectPseudoIdentifier .
      ?lab_res a sphn:LabResult .
      
      ?lab_res sphn:hasSubjectPseudoIdentifier ?patient .
      ?lab_res sphn:hasLabResultLabTestCode ?test_code .
      ?test_code rdf:type loinc:6690-2 .
  }

.. _Figure 17:
.. image:: ../images/sparql/Results_Patient_with_measurements_of_Leukocytes_in_Blood_by_Automated_count.png
   :align: center
   :alt: : Excerpt of results of running the SPARQL query for Patient with measurements of Leukocytes in Blood by Automated count in GraphDB on the mock-data.
   
**Figure 17: Excerpt of results of running the SPARQL query for Patient with measurements of Leukocytes in Blood by Automated count in GraphDB on the mock-data.**


6. Min/max values for measurements of Leukocytes in Blood by Automated count (LOINC 6690-2)
*******************************************************************************************


Here, the question is what are the min/max values measured for a 
lab test identified by a specific LOINC code. 
To address this question for 
`measurements of Leukocytes in Blood by Automated count <https://loinc.org/rdf/6690-2>`_ , 
the previous query can be modified as follow:

.. code-block:: sparql

   PREFIX rdf:<http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
   PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#> 
   PREFIX sphn:<https://biomedit.ch/rdf/sphn-ontology/sphn#> 
   PREFIX resource:<https://biomedit.ch/rdf/sphn-resource/> 
   PREFIX xsd:<http://www.w3.org/2001/XMLSchema#>
   PREFIX loinc: <https://loinc.org/rdf/>
   PREFIX sphn-loinc: <https://biomedit.ch/rdf/sphn-resource/loinc/>
   
  SELECT ?test_code (MIN(?lab_res_value) AS ?min_value) (MAX(?lab_res_value) AS ?max_value) ?lab_res_unit
   WHERE {
       ?lab_res a sphn:LabResult .
       
       ?lab_res sphn:hasLabResultLabTestCode ?test_code . 
       ?test_code rdf:type loinc:6690-2 .
       
       ?lab_res sphn:hasLabResultValue ?lab_res_value .
       ?lab_res sphn:hasLabResultUnit ?lab_res_unit.
   }
   GROUP BY ?test_code ?lab_res_unit

Running this query in GraphDB with the mock-data used throughout this guide produces 
the result shown in `Figure 18`_. Note the use of the :code:`GROUP BY ...` 
statement to group the retrieved results.


.. _Figure 18:
.. image:: ../images/sparql/Results_minmax_measurements_of_Leukocytes_in_Blood_by_Automated_count.png
   :align: center
   :alt: : Results of running the SPARQL query for min/max values for measurements of Leukocytes in Blood by Automated count (LOINC 6690-2) in GraphDB on the mock-data.
   
**Figure 18: Results of running the SPARQL query for min/max values for measurements of Leukocytes in Blood by Automated count (LOINC 6690-2) in GraphDB on the mock-data.**


References
----------

Further information is available in the following references: 

* `SPARQL Documentation <https://www.w3.org/TR/sparql11-query/>`_

* `SPHN Semantic Framework <https://sphn-semantic-framework.readthedocs.io/>`_

* :ref:`SPHN LOINC <external-terminologies-loinc>`

* :ref:`SPHN SNOMED-CT <external-terminologies-snomed-ct>`
 
