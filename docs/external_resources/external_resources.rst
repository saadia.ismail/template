.. _external-terminologies:

External terminologies
======================

SPHN provides the external terminologies via the 
:ref:`framework-terminology-service`. Currently, the following terminologies 
are provided:

* :ref:`external-terminologies-chop` (including historical version since 2016)
* :ref:`external-terminologies-icd` (including historical version since 2014)
* :ref:`external-terminologies-snomed-ct` (including historical version since 2021-01-31; note that only the January and July releases are provided in RDF format.)
* :ref:`external-terminologies-loinc` (including historical version since version 2.69)
* :ref:`external-terminologies-atc` (including historical version since 2016)
* :ref:`external-terminologies-ucum` 


If you are missing a terminology important for your project, write a request to 
dcc@sib.swiss.