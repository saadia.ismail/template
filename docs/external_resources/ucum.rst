.. _external-terminologies-ucum:

UCUM
=================
.. note:: 
 To find out more watch the `Semantic standards training <https://sphn.ch/training/semantic-standards-training/>`_ 
 
Introduction to the standard 
----------------------------------

The Unified Code for Units of Measure (UCUM, https://ucum.org/trac) is a compositional code system intended to include all units of measures being contemporarily used in international science, engineering, and business. UCUM units can be composed by using the UCUM expression syntax. UCUM, similarly to LOINC, is released and maintained by the Regenstrief Institute. It is adopted by standard organization such as DICOM (https://www.dicomstandard.org/) or HL7 (https://www.hl7.org/) and is strongly encouraged by LOINC (https://loinc.org/). UCUM is used as one recommended standard for the SPHN concept ``Unit`` (https://biomedit.ch/rdf/sphn-ontology/sphn/unit).


Technical specification 
------------------------------------

Next to standard units such as L=liters, g=grams, UCUM allows for: 

* Prefixes e.g. ``k`` for 'kilo' or ``u`` for 'micro'

* Annotations in ``{}`` e.g. ``mU/g {Hgb}`` for 'miliUnits per gram Hemoglobin'

* Scaling factors such as ``10*X`` e.g. ``10*3/ul`` for 'thousand per microliter'

* Lexical elements in ``[]`` e.g. ``mm[Hg]`` for  'millimeters of mercury'

* Operators e.g. ``.`` for 'multiplications' and ``/`` for 'division'


Implementation in RDF for SPHN 
-------------------------------
For the SPHN RDF file, we did not implement full support for the syntax. We rely on a large library of pre-built valid UCUM codes from the National Library of Medicine, National Institutes of Health, U.S. Department of Health and Human Services with content contributions from Intermountain Healthcare and the Regenstrief Institute (`download list, <https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiBo_2Q2ZvwAhUp8LsIHfJmB8sQFjABegQIAxAD&url=https%3A%2F%2Fucum.org%2Ftrac%2Fraw-attachment%2Fwiki%2Fadoption%2Fcommon%2FTableOfExampleUcumCodesForElectronicMessaging.xlsx&usg=AOvVaw28PFyAjKDUXtLqZda_vj9Chttps://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiBo_2Q2ZvwAhUp8LsIHfJmB8sQFjABegQIAxAD&url=https%3A%2F%2Fucum.org%2Ftrac%2Fraw-attachment%2Fwiki%2Fadoption%2Fcommon%2FTableOfExampleUcumCodesForElectronicMessaging.xlsx&usg=AOvVaw28PFyAjKDUXtLqZda_vj9C>`_ and `additional information <https://ucum.org/trac/wiki/adoption/common>`_, Version 1.5, Released 06/2020.).

In addition, a couple of units from the SPHN dataset (e.g. ``cGy``, ``MBq``, ``mCi``), not included in this library, has been added.

The namespace used in the RDF to refer to the UCUM terms is: <http://biomedit.ch/sphn-resource/ucum/>.

Since UCUM does not have a release cycle, the versioning relies on the year when the RDF is generated. The version IRI is provided for each version of UCUM in RDF (e.g., https://biomedit.ch/rdf/sphn-resource/ucum/2021/1/), which is composed of the namespace followed by the year the RDF is being generated and the version of the RDF generated for that year. 

In this RDF, a root class is generated (https://biomedit.ch/rdf/sphn-resource/ucum/UCUM). The UCUM codes are provided as ``owl:NamedIndividuals``, with a ``rdfs:label``, a ``rdfs:comment`` and are linked to the ``ucum:UCUM`` root class (see example below of the unit kilogram)::

  ucum:kg a owl:NamedIndividual,
        ucum:UCUM ;
    rdfs:label "kg" ;
    rdfs:comment "kilogram" .

All individuals are directly usable in any project as instance data. 


This release contains a flat list of the UCUM terms in RDF. There hasn’t been any hierarchy established to cluster the terms under different classes. 

If you need an UCUM code not present in the list, please submit a request to the DCC dcc@sib.swiss, we will then include it in the next SPHN UCUM RDF release.




Availability and usage rights
------------------------------
The RDF file is available via the :ref:`framework-terminology-service`.

The copyright follows the instructions provided by Regenstrief Institute. UCUM is Copyright © 1999-2013 Regenstrief Institute, Inc. and The UCUM Organization, Indianapolis, IN. All rights reserved. See `TermsOfUse <http://unitsofmeasure.org/trac/wiki/TermsOfUse>`_.

