.. _external-terminologies-chop:

CHOP
=========================
.. note:: 
 To find out more watch the `Semantic standards training <https://sphn.ch/training/semantic-standards-training/>`_ 
 
Introduction to classification 
---------------------------------------

The “Schweizerische Operationsklassifikation” (CHOP) is a Swiss medical coding system for procedures performed in hospitals. CHOP covers a wide range of procedures including surgical, radiotherapeutic, diagnostic procedure such as ECGs, and procedures for patient activation (e.g. walking up and down stairs) as well as planning activities. CHOP, initially based on the ICD-9-CM, is a Swiss classification used mainly for billing purposes. CHOP versions are released annualy by the Federal Statistical Office (FSO) and are free to download. The CHOP classification is available in `German <https://www.bfs.admin.ch/bfs/de/home/statistiken/gesundheit/nomenklaturen/medkk/instrumente-medizinische-kodierung.assetdetail.9286150.html>`_, `French <https://www.bfs.admin.ch/bfs/fr/home/statistiques/sante/nomenclatures/medkk/instruments-codage-medical.html>`_ and `Italian <https://www.bfs.admin.ch/bfs/it/home/statistiche/salute/nomenclature/medkk/strumenti-codifica-medica.html>`_. 

The hierarchy of CHOP is divided into six levels of classification (see example in Figure 1): 

* The code of the first level starts with a “C” followed by a number and corresponds to the chapter. 
* The code of each of the other levels starts with either a number or a letter. 

.. image:: ../images/chop_hierarchy.*
   :height: 300
   :align: center
   :alt: CHOP hierarchy
   
**Figure 1. Example of CHOP hierarchy.** The code ``C0`` represents the most generic level of information, corresponding to a chapter while the code ``00.12.00`` is the most specific level of information. The different levels enable building up the hierarchy of the classification: the lower the level; the more specific the information. 

 

Information for use in data science
------------------------------------

CHOP classifies  procedures into one of 19 different categories (chapters). The chapters are largely differentiated according to medical specialty. For example, chapter C1 contains procedures on the nervous system, chapter C2 contains procedures on the endocrine system. Chapter C16 contains various diagnostic and therapeutic procedures across all therapeutic areas, e.g. radiography of the spine (``87.2``) and CT of the thorax (``87.41``). Chapter C17 contains measurement instruments, e.g. scores such as mobility scores. Chapter 18 is dedicated to rehabilitation, and chapter C0 contains procedures and interventions not classifiable elsewhere. The hierarchies can support data aggregation but, depending on the scope of aggregation, several chapters might need to be taken into account.  

 

Implementation in RDF for SPHN 
--------------------------------

The CHOP coding system is available as a multi-language Excel file in which the correspondence between the German, French and Italian versions are presented. This file has been used (saved in Excel format) to generate the RDF representation via a Python script.  

The namespace used in the RDF is: <https://biomedit.ch/rdf/sphn-resource/chop/>.
 
A version IRI is provided for each version of CHOP in RDF (e.g., https://biomedit.ch/rdf/sphn-resource/chop/2020/1/), composed of the namespace followed by the ``year`` of release of CHOP and the ``version`` of the RDF generated for that release. 

In the RDF version generated of CHOP, three types of information are stored:

1. The CHOP code itself, which is defined as a class (``rdfs:class``). 
2. The name (label) of the CHOP code, represented by the property ``rdfs:label`` corresponding to the row where the item type is “T” (here "T" stands for “title”). The label is described in each of the three languages with the suffix ``@DE`` for German, ``@FR`` for French and ``@IT`` for Italian. 
3. The hierarchy of the codes, represented by the property ``rdfs:subClassOf``.  

 

The following information provided by CHOP are not currently incorporated in SPHN CHOP RDF:

* Item type B, I, N, S and X 
* Codable code  
* Indent level 
* Laterality 

CHOP versions from 2016 to 2022 have been translated into RDF.

Availability and usage rights
------------------------------

The CHOP RDF file is available via the :ref:`framework-terminology-service`. 

The copyright follows the instructions provided by FSO, Neuchâtel 2022: “Wiedergabe unter Angabe der Quelle für nichtkommerzielle Nutzung gestattet” (i.e., Reproduction is authorized, except for commercial purposes, if the source is acknowledged). 
