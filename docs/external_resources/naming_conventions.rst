.. _external-terminologies-naming-conventions:

Naming conventions
==================

Versions of external resources used in the context of the SPHN infrastructure are uniquely identified with a code composed of the resource short name and the version number of the resource. This code should be used whenever it is necessary to identify the exact version of the resource being referenced.

.. list-table:: Naming conventions for coding systems and versions
   :widths: 15 40 25 20
   :header-rows: 1

   * - short name
     - full name
     - coding system and version
     - examples
   * - :ref:`external-terminologies-atc`
     - Anatomical Therapeutic Chemical classification
     - ATC-[YEAR]
     - ATC-2020
   * - :ref:`external-terminologies-chop`
     - Swiss Classification Of Procedures
     - CHOP-[YEAR]
     - CHOP-2019
   * - EMDN
     - European Medical Device Nomenclature
     - EMDN-[VERSION]
     - EMDN-1.1
   * - GMDN
     - Global Medical Device Nomenclature    
     - GMDN
     - GMDN
   * - GTIN
     - Global Trace Item Number
     - GTIN
     - GTIN
   * - GUDID
     - Global Unique Device Identification Database
     - GUDID
     - GUDID
   * - :ref:`external-terminologies-icd`
     - International Statistical Classification of Diseases and Related Health Problems
     - ICD-10-GM-[YEAR]
     - ICD-10-GM-2018
   * - ICD-O-3
     - International Classification of Diseases for Oncology
     - ICD-O-3.[REVISION]
     - ICD-O-3.0; ICD-O-3.1; ICD-O-3.2
   * - ICPC
     - International Classification of Primary Care
     - ICPC-[VERSION]-[YEAR]
     - ICPC-2-2003
   * - L4CHLAB
     - LOINC for Swiss Laboratories
     - L4CHLAB-[YEAR].[#]
     - L4CHLAB-2019.1
   * - :ref:`external-terminologies-loinc`
     - Logical Observation Identifier Names and Codes
     - LOINC-[VERSION]
     - LOINC-2.67
   * - MedDRA
     - Medical Dictionary for Regulatory Activities
     - MedDRA-[VERSION]
     - MedDRA-23.1
   * - NANDA
     - NANDA Nursing Diagnoses
     - NANDA-[YEAR-YEAR]
     - NANDA-2018-2020
   * - ORPHA
     - Orphanet nomenclature of rare diseases
     - ORPHA-[YEAR-MONTH]
     - ORPHA-2021-07  
   * - :ref:`external-terminologies-snomed-ct`
     - Systematized Nomenclature of Medicine Clinical Terms
     - SNOMED-CT-[YEAR-MONTH-DAY]
     - SNOMED-CT-2021-01-31
   * - UCUM
     - Unified Code for Units of Measure
     - UCUM
     - UCUM
   * - UID
     - Swiss unique enterprise identification number
     - UID
     - UID


The sections below highlights the different namespaces (ontology and version IRIs) 
given for each external terminologies provided in the 
:ref:`framework-terminology-service`. In addition, the IRI used for encoding 
the actual codes are also expressed in the section **IRI for codes**. 
The version of the terminology being used in the different SPHN RDF schemas is 
highlighted in the **Version IRI**.


ATC namespace
-------------

**Ontology IRI**:

:code:`https://biomedit.ch/rdf/sphn-resource/atc/`

**Version IRI** (available from version 2016 to 2022):

:code:`https://biomedit.ch/rdf/sphn-resource/atc/2022/1` *(imported in SPHN 2022.1 and 2022.2)* 

:code:`https://biomedit.ch/rdf/sphn-resource/atc/2021/3` *(imported in SPHN 2021.1 and 2021.2)* 

:code:`https://biomedit.ch/rdf/sphn-resource/atc/2020/1` 

:code:`https://biomedit.ch/rdf/sphn-resource/atc/2019/1` 

:code:`https://biomedit.ch/rdf/sphn-resource/atc/2018/1` 

:code:`https://biomedit.ch/rdf/sphn-resource/atc/2017/1` 

:code:`https://biomedit.ch/rdf/sphn-resource/atc/2016/1` 

**IRI for codes** (to be used by data providers when annotating data): 

:code:`https://www.whocc.no/atc_ddd_index/?code=`


CHOP namespace
--------------

**Ontology IRI**:
:code:`https://biomedit.ch/rdf/sphn-resource/chop/`

**Version IRI** (available from version 2016 to 2022):

:code:`https://biomedit.ch/rdf/sphn-resource/chop/2022/4` *(imported in SPHN 2022.1)* 

:code:`https://biomedit.ch/rdf/sphn-resource/chop/2021/4`

:code:`https://biomedit.ch/rdf/sphn-resource/chop/2020/4` 

:code:`https://biomedit.ch/rdf/sphn-resource/chop/2019/4` 

:code:`https://biomedit.ch/rdf/sphn-resource/chop/2018/4` 

:code:`https://biomedit.ch/rdf/sphn-resource/chop/2017/4` 

:code:`https://biomedit.ch/rdf/sphn-resource/chop/2016/4` 

**IRI for codes** (to be used by data providers when annotating data): 

:code:`https://biomedit.ch/rdf/sphn-resource/chop/`


ICD-10-GM namespace
-------------------

**Ontology IRI**:

:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/`

**Version IRI** (available from version 2014 to 2022):

:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2022/2` *(imported in SPHN 2022.1 and 2022.2)* 

:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2021/2` *(imported in SPHN 2021.1 and 2021.2)* 

:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2020/2` 

:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2019/2` 

:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2018/2` 

:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2017/2` 

:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2016/2` 

:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2015/2` 

:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2014/2` 

**IRI for codes** (to be used by data providers when annotating data):

:code:`https://biomedit.ch/rdf/sphn-resource/icd-10-gm/`


LOINC namespace
---------------

**Ontology IRI**:

:code:`https://biomedit.ch/rdf/sphn-resource/loinc/`

**Version IRI** (available from version 2.69 to 2.72):

:code:`https://biomedit.ch/rdf/sphn-resource/loinc/2.72/1` *(imported in SPHN 2022.1 and 2022.2)*

:code:`https://biomedit.ch/rdf/sphn-resource/loinc/2.71/1`

:code:`https://biomedit.ch/rdf/sphn-resource/loinc/2.69/1` *(imported in SPHN 2021.1 and 2021.2)*

**IRI for codes** (to be used by data providers when annotating data):

:code:`https://loinc.org/rdf/`


SNOMED CT namespace
-------------------

**Ontology IRI**:

:code:`http://snomed.info/sct/900000000000207008#`

**Version IRI** (available from version 01.2021 to 01.2022): 

:code:`http://snomed.info/sct/900000000000207008/version/20220207` *(imported in SPHN 2022.1 and 2022.2)* 

:code:`http://snomed.info/sct/900000000000207008/version/20210805` 

:code:`http://snomed.info/sct/900000000000207008/version/20210304` *(imported in SPHN 2021.1 and 2021.2)* 

**IRI for codes** (to be used by data providers when annotating data): 

:code:`http://snomed.info/id/`


UCUM namespace
--------------

**Ontology IRI**:  

:code:`https://biomedit.ch/rdf/sphn-resource/ucum/`

**Version IRI**:

:code:`https://biomedit.ch/rdf/sphn-resource/ucum/2021/1` *(imported in SPHN 2022.1, 2022.2 2021.1 and 2021.2)*

**IRI for codes** (to be used by data providers when annotating data):

:code:`https://biomedit.ch/rdf/sphn-resource/ucum/`

