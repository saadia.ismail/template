.. _external-terminologies-atc:

ATC
===
.. note:: 
 To find out more watch the `Semantic standards training <https://sphn.ch/training/semantic-standards-training/>`_  
 
Introduction to the classification 
--------------------------------------

The Anatomical Therapeutic Classification (ATC, https://www.whocc.no/atc_ddd_index/) is a drug classification that categorizes active substances of drugs according to the organ or system where their therapeutic effect occurs. ATC is provided by the World Health Organization Collaborating Centre for Drug Statistics Methodology (WHOCC) and is updated once a year.  In Switzerland, the ATC code is used by Swissmedic (https://www.swissmedic.ch) in the extended list of medicinal products approved by Swissmedic. 

 

The hierarchy of ATC is divided in five levels of classification (see Figure 1): 

* Level 1: Anatomical main group (represented by one letter) 
* Level 2: Therapeutic subgroup (represented by two digits) 
* Level 3: Pharmacological subgroup (represented by one letter) 
* Level 4: Chemical subgroup (represented by one letter) 
* Level 5: Chemical substance (represented by two digits) 

 
.. image:: ../images/atc_code_example.*
   :height: 300
   :align: center
   :alt: ATC code example
   
Figure 1. Example of the ATC code A02AB03. The different levels and their meaning (label) are presented for the ATC code A02AB03 which corresponds to the aluminium phosphate drug. 

 

ATC is used as one recommended standard for the SPHN concept: Substance (https://biomedit.ch/rdf/sphn-ontology/sphn/substance). 

Information for use in data science 
------------------------------------------

The following information is taken from the 2021 Guidelines that are available on the ATC Website https://www.whocc.no/atc_ddd_index/. For more information, please consult the Guideline document. 

Many medicines are used and approved for two or more indications, while normally only one ATC code will be assigned. Besides, ATC codes are often assigned according to the mechanism of action rather than therapy. An ATC group may therefore include medicines with many different indications, and drugs with similar therapeutic use may be classified in different groups. 

A medicinal substance can be given more than one ATC code if it is available in two or more strengths or routes of administration with clearly different therapeutic uses (e.g. Finasteride). 

Implementation in RDF for SPHN
------------------------------

The ATC code (English version) made available by WHOCC in an Excel file has been translated into an RDF representation via a Python script. 

Since ATC is not providing a generic code for grouping all ATC codes together, an ``ATC`` class has been created using the namespace <https://biomedit.ch/rdf/sphn-resource/atc/> (see Figure 2). A version IRI is provided for each version of ATC in RDF (e.g., https://biomedit.ch/rdf/sphn-resource/atc/2021/1/), which is composed of the namespace followed by the ``year`` of release of ATC and the ``version`` of the RDF generated for that release. 


The ``ATC`` class puts together ATC codes in the RDF provided by WHOCC (see again Figure 2). These ATC codes use the namespace following the ATC website link: <https://www.whocc.no/atc_ddd_index/?code=>, enabling the codes to be dereferencable on the web. When data is referring to an ATC code, it must use this link as namespace. 

.. image:: ../images/atc_namespace.png
   :align: center
   :alt: ATC namespace

**Figure 2. Grouping of ATC codes under the 'ATC' class.** All ATC codes are grouped under the SPHN defined class ATC (https://biomedit.ch/rdf/sphn-resource/atc/ATC).

In the ATC RDF, three types of information are stored (see Figure 3): 

1. The ATC code itself, which is defined as a class, 
2. The name (label) of the ATC code, represented with the use of the property ``rdfs:label``, 
3. The hierarchy of the codes, represented with the use of the property ``rdfs:subClassOf`` (see Figure 4). Level 1 defines the main classes. Level 2 codes are sub-classes of Level 1. Level 3 are sub-classes of Level 2. Level 4 are sub-classes of Level 3. Level 5 are sub-classes of Level 4.  

 

.. image:: ../images/atc_rdf_class.*
   :height: 300
   :align: center
   :alt: ATC rdf class
   
**Figure 3. RDF representation of an ATC code.** A code is defined as an RDF Class and is associated with a specific label. In addition, a code A can be linked to another code B as a subclass of the latter (i.e., code A is a child of code B and code B is a parent of code A). 


 
.. image:: ../images/atc_rdf_subclass.*
   :height: 300
   :align: center
   :alt: ATC rdf subclass
   
**Figure 4. Representation of the relationship between ATC codes in RDF.** The property ``subClassOf`` defines whether a specific ATC code is a child of another ATC code. For instance, level 5 ATC codes are defined as sub-classes of level 4 ATC codes and level 4 ATC codes are sub-classes of level 3 ATC codes. With the reasoning possibilities offered in RDF, one can infer that a level 5 ATC code is a sub-class of a level 1 ATC code.  


The following information provided by ATC are not incorporated in RDF, either because they are not relevant or are reflected in other concepts of SPHN: 

* The DDD (average maintenance dose per day of a substance) 
* The Unit (unit of the dose) 
* The Administration route (route of administration of the substance) 
* The Comment (additional comment about a substance). 

Availability and usage rights
-----------------------------

The ATC RDF file is available via the :ref:`framework-terminology-service`.

The copyright follows the instructions provided by WHO regarding ATC (https://www.whocc.no/copyright_disclaimer/): “Use of all or parts of the material requires reference to the WHO Collaborating Centre for Drug Statistics Methodology. Copying and distribution for commercial purposes is not allowed. Changing or manipulating the material is not allowed.” 

