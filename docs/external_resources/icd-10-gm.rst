.. _external-terminologies-icd:

ICD-10-GM
=========================
.. note:: 
 To find out more watch the `Semantic standards training <https://sphn.ch/training/semantic-standards-training/>`_ 
 
Introduction to the classification
-----------------------------------
Coding of medical care diagnoses in Switzerland uses the International Statistical Classification Of Diseases And Related Health Problems, 10th revision, German Modification (ICD-10-GM). The German Modification is based on the WHO version and is produced (and updated annulay) by the German Federal Institute of Drugs and Medical Devices (BfArM). It is published on the `BfArM Website <https://www.dimdi.de/dynamic/de/klassifikationen/icd/>`_. In Switzerland, a new version is adopted every two years. The Swiss Federal Office of Statistics (BFS) publishes  French and Italian language versions of the ICD-10-GM.



The hierarchy of ICD-10-GM is divided into five levels of classification, as shown in Figure 1, with the top level being the most generic and the lowest level the most specific:

* ``Kapitel`` (chapter) represents the highest level. There are 22 chapters in ICD-10-GM 2021,
* ``Gruppe`` (block) represent the second level of information corresponding to a code range associated with a chapter,
* ``Kategorie, Dreistelle`` (category) represents a three-character code which is the central element of ICD-10-GM,
* ``Subkategorie, Viersteller`` (subcategory) represents a four-character code separated by a decimal point,
* ``Subkategorie, Fünfsteller`` (subcategory) represents a five-character code and is the most specific level of information possible.

.. image:: ../images/icd-10-gm_hierarchy.*
   :height: 300
   :align: center
   :alt: ICD-10-GM hierarchy

**Figure 1. Example of ICD-10-GM hierarchy.** The chapter code ``17`` represents the most generic level of information while the code ``Q89.00`` is the most specific level of information corresponding to a fifth level code subcategory. The different levels enable building up the hierarchy of the classification: the lower the level the more specific the information. 


Note: A three-character code can have a four- or five-character code as children.

Information for use in data science
-----------------------------------
ICD-10 is primarily designed to support statistical analysis. ICD-10-GM as well as the WHO version is organized as a mono-hierarchy and provides a classification of diagnoses based on 22 different broad classes of diseases (chapters) numbered using Roman numerals. Each chapter is subdivided into several more specific categories. For example:

*	chapter I contains certain infectious and parasitic diseases,
*	chapter X contains diseases of the respiratory system and
*	chapter XI contains diseases of the digestive system.

Each disease is allocated to only one category. That is, an infectious disease that affects the respiratory and digestive systems is allocated to only one of these 3 chapters. For example, respiratory tuberculosis (code A16) can be found in chapter I, while influenza (codes J09-J18) can be found in chapter X.  ICD-10 is therefore useful for any analysis in which it is important that each disease is counted only once. It is less useful for any analyses where there is a need to identify all concepts with a defined meaning. Based on the example above, a query to identify all respiratory infectious diseases would need to include both chapters I and X.


Implementation in RDF for SPHN
------------------------------
The ICD-10-GM is made available in three different CSV files containing altogether the five levels of information (``icd10gm2021syst_kapitel.txt``, ``icd10gm2021syst_gruppen.txt`` and ``icd10gm2021syst_kodes.txt``). These files have been parsed (via a Python script) to reconstruct and translate the ICD-10 hierarchy into an RDF representation. Only the German language version of ICD-10-GM has been translated into RDF.

The namespace used is: <https://biomedit.ch/rdf/sphn-resource/icd-10-gm/>

A version IRI is provided for each version of ICD-10-GM in RDF (e.g. https://biomedit.ch/rdf/sphn-resource/icd-10-gm/2021/1/), which is composed of the namespace followed by the ``year`` of release of ICD-10-GM and the ``version`` of the RDF generated for that release.

In the ICD-10-GM RDF, three types of information are stored:

1. The ICD-10-GM code itself, which is defined as a class (``rdfs:class``),
2. The name (label) of the ICD-10-GM code, represented by the property ``rdfs:label``,
3. The hierarchy of the codes, represented by the property ``rdfs:subClassOf``. 


ICD-10-GM versions from 2014 to 2021 have been translated into RDF.

Availability and usage rights
-----------------------------

The ICD-10-GM RDF file is available via the :ref:`framework-terminology-service`.

The ICD-10-GM RDF file has been produced using the ICD-10-GM machine-readable 
version of the German Institute of Medical Documentation and Information (DIMDI) 
which belongs to the BfArM. 
