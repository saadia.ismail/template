.. _external-terminologies-loinc:

LOINC
=========================
.. note:: 
 To find out more watch the `Semantic standards training <https://sphn.ch/training/semantic-standards-training/>`_ 
 
Introduction to the classification 
----------------------------------------
Logical Observation Identifier Names and Codes (LOINC, https://loinc.org/) is a common language for identifying health measurements, observations, and documents. LOINC is released twice a year by the Regenstrief Institute, a non-profit clinical research organization. LOINC is publicly available at no cost and provides universal codes and descriptions allowing humans and machines to uniquely identify laboratory tests (e.g. Fasting glucose in Capillary blood) and clinical observations (e.g. Heart rate). With the LOINC Document Ontology, LOINC provides consistent semantics for documents exchanged between systems. The LOINC Document Ontology is not currently used in SPHN.  

A LOINC code is made up of the elements shown in Figure 1: 

* ``Component``: the substance or entity being measured or observed, 
* ``Property``: the characteristic or attribute of the analyte,  
* ``Time``: the timing aspect of the measurement or observation,  
* ``System``: the specimen or thing upon which the observation was made,  
* ``Scale``: how the observation value is quantified or expressed: quantitative, ordinal, nominal,  
* ``Method``:  a high-level classification of how the observation was made (optional). 

.. image:: ../images/loinc_hierarchy.*
   :height: 300
   :align: center
   :alt: LOINC hierarchy
   
**Figure 1. Example of the LOINC code 2340-8.** The six different types of information provided by the code are represented. When put in a certain order, these information make up the LOINC Fully-Specified Name (FSN). 

LOINC is used for meaning binding of SPHN concepts and as a recommended standard for the lab test code (http://biomedit.ch/rdf/sphn-ontology/sphn#hasLabResultLabTestCode) provided as contextual information of the SPHN concept ``Lab Result`` (https://biomedit.ch/rdf/sphn-ontology/sphn/LabResult). 


Information for use in data science 
---------------------------------------

Multicenter research projects using LOINC coded data from different sources can identify similarities and differences in those data. The meaning of the data is readable from the information behind the LOINC code. Using SPARQL queries (see :ref:`sparql_query`) a researcher can access this information and gains an important prerequisite for evaluating the comparability of the data. 

Example 1: The following LOINC coded data are provided by two different data providers:

* Data provider A: 224 records with LOINC code ``718-7 Hemoglobin [Mass/volume] in Blood``;
* Data provider B: 310 records with LOINC code ``55782-7 Hemoglobin [Mass/volume] in Blood by Oximetry``.

Both sets of data come from the measurement of Hemoglobin in Blood in mass concentration. Five of the six elements are directly comparable. The difference between the two sets of data is that data provider A did not specify the measurement method in the code, while data provider B explicitly specified the method as (``Oximetry``). Whether the results of both laboratory tests can be considered and compared to answer a research question depends on the granularity of information needed.


Example 2: The following LOINC coded data are provided by two different data providers:

* Data provider A: 95 records with LOINC code ``1556-0 Fasting glucose [Mass/volume] in Capillary blood``;
* Data provider B: 110 records with LOINC code ``51596-5 Glucose [Moles/volume] in Capillary blood``.

Assuming that there is no further data on eating behavior prior to glucose measurement available the results of the two laboratory tests would not be comparable. The data from data provider A refer to glucose tests taken from patients not allowed to eat anything before the test, while the test information from data provider B does not reveal the pre-conditions (if any). Additional data points outside the LOINC code from data provider B would be needed to assess comparability.

LOINC does not provide codes for machines or test kits and this may also have an impact on the comparability of laboratory test results.

Implementation in RDF for SPHN
------------------------------

The full list of LOINC terms is available as a table at https://loinc.org/downloads/loinc-table/. The LOINC Table Core has been used to generate the RDF representation via a Python script.  

The namespace used in the RDF to refer to the LOINC terms is: <https://loinc.org/rdf/>. Each LOINC term is therefore already dereferenceable since the namespace connects to the official webpage of the LOINC classification. Data providers must use this namespace when referring to LOINC terms.

A version IRI is provided for each version of LOINC in RDF (e.g., https://biomedit.ch/rdf/sphn-resource/loinc/2.69/1/), which is composed of the namespace followed by the ``version`` of release of LOINC and the ``version`` of the RDF generated for that release. 


However, since LOINC is not providing a generic code for grouping all LOINC terms together, a ``LOINC`` class has been created using the namespace <https://biomedit.ch/rdf/sphn-resource/loinc/>. A version IRI is provided for each version of LOINC in RDF (e.g., https://biomedit.ch/rdf/sphn-resource/loinc/2021/1/), which is composed of the namespace followed by the year of release of LOINC and the version of the RDF generated for that release.

Note also that LOINC provides metadata with 6-axis of contextual information for each term. These contextual information have been incorporated in RDF leading to the creation of six properties (e.g., ‘hasComponent’, ‘hasMethodType’) using the SPHN LOINC namespace, <https://biomedit.ch/rdf/sphn-resource/loinc/>. This means that the data user must carefully use the namespaces: 

* ``https://loinc.org/rdf/`` when querying LOINC terms; 
* ``https://biomedit.ch/rdf/sphn-resource/loinc/`` when referring to the 6-axis properties.


This release contains a flat list of the LOINC terms in RDF. No hierarchy has been established to cluster the terms under different classes. 

 .. _sparql_query:

Example of a SPARQL query
-------------------------

Many LOINC codes representing ``Glucose in blood`` are available. Using the LOINC Part (https://loinc.org/get-started/loinc-term-basics/), it is possible to query all LOINC codes which have the Component ``Glucose`` and the System ``Blood`` called ``Bld`` in LOINC: 

.. code-block:: sparql

  PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
  PREFIX sphn-loinc: <https://biomedit.ch/rdf/sphn-resource/loinc/>
  select ?iri ?name_of_the_code
  where {
  ?iri rdfs:label ?name_of_the_code .
  ?iri sphn-loinc:hasComponent "Glucose" .
  ?iri sphn-loinc:hasSystem "Bld" .
  } limit 100 



.. list-table:: Results of the query:
   :widths: 25 50
   :header-rows: 1

   * - ?iri
     - ?name_of_the_code
   * - loinc:15074-8
     - "Glucose [Moles/volume] in Blood"
   * - loinc:2339-0
     - "Glucose [Mass/volume] in Blood"
   * - loinc:2340-8 
     - "Glucose [Mass/volume] in Blood by Automated test strip"
   * - loinc:2341-6 
     - "Glucose [Mass/volume] in Blood by Test strip manual"
   * - loinc:5914-7 
     - "Glucose [Presence] in Blood by Test strip"
   * - loinc:72516-8 
     - "Glucose [Moles/volume] in Blood by Automated test strip"


Availability and usage rights
------------------------------

The LOINC RDF file is available via the 
:ref:`framework-terminology-service`.

The copyright follows the instructions provided by LOINC (http://loinc.org). LOINC is copyright © 1995-2022, Regenstrief Institute, Inc. and the Logical Observation Identifiers Names and Codes (LOINC) Committee and is available at no cost under the license at http://loinc.org/license. LOINC® is a registered United States trademark of Regenstrief Institute, Inc. 
