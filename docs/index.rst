SPHN Semantic Framework
==================================================

.. toctree::
  :maxdepth: 2
  :caption: SPHN framework

  sphn_framework/introduction
  sphn_framework/sphndataset
  sphn_framework/sphnrdfschema
  sphn_framework/shacler
  sphn_framework/sparqler
  sphn_framework/dataquality
  sphn_framework/terminology_service


.. toctree::
  :maxdepth: 1
  :caption: External resources used
  
  external_resources/external_resources
  external_resources/naming_conventions
  external_resources/atc
  external_resources/chop
  external_resources/icd-10-gm
  external_resources/loinc
  external_resources/snomed-ct
  external_resources/ucum
  external_resources/other_resources

  
.. toctree::
  :maxdepth: 2
  :caption: User guide - How to?
  
  user_guide/access_use_sphn_schema
  user_guide/ontology_generation
  user_guide/data_generation
  user_guide/data_quality
  user_guide/shacler
  user_guide/data_exploration
  user_guide/sparql
  user_guide/sparqler
  user_guide/data_analysis
  user_guide/terminology_service
  user_guide/project_terminologies

.. toctree::
  :maxdepth: 2
  :caption: Examples of Implementation

  examples_of_implementation/chuv_with_oracle
  examples_of_implementation/usb_sphn_export
  examples_of_implementation/usz_early_version_pipeline
  examples_of_implementation/usz_strategic_implementation

.. toctree::
  :maxdepth: 2
  :caption: Background information

  background_information/clinical_interoperability_background
  background_information/semantic_web
  background_information/rdf_background
  background_information/shacl_background
  background_information/sparql_background
  
.. toctree::
  :maxdepth: 2
  :caption: Other
  
  other/contactinformation
  other/acknowledgements
